const fastify = require('fastify')();

const addic7ed = require('addic7ed-scraper');

const config = require('./config');

fastify.register(require('fastify-cors'));

fastify.get('/', async (request, reply) => {
    reply.send(await addic7ed.getShows());
});

fastify.get('/show/:showId', async (request, reply) => {
    const { showId } = request.params;
    reply.send(await addic7ed.getSeasons(showId));
});

fastify.get('/show/:showId/season/:seasonIndex', async (request, reply) => {
    const { showId, seasonIndex } = request.params;
    reply.send(await addic7ed.getEpisodes(showId, seasonIndex));
});

fastify.get('/show/:showId/season/:seasonIndex/episode/:episodeIndex', async (request, reply) => {
    const { showId, seasonIndex, episodeIndex, episodeSlug } = request.params;
    reply.send(await addic7ed.getEpisode({ showId }, seasonIndex, episodeIndex, episodeSlug));
});

fastify.get('/show/:showSlug/season/:seasonIndex/episode/:episodeIndex/:episodeSlug', async (request, reply) => {
    const { showSlug, seasonIndex, episodeIndex, episodeSlug } = request.params;
    reply.send(await addic7ed.getEpisode({ showSlug }, seasonIndex, episodeIndex, episodeSlug));
});

fastify.get('/movie/:movieId', async (request, reply) => {
    const { movieId } = request.params;
    reply.send(await addic7ed.getMovie(movieId));
});

fastify.get('/comments/:episodeOrMovieId', async (request, reply) => {
    const { episodeOrMovieId } = request.params;
    reply.send(await addic7ed.getComments(episodeOrMovieId));
});

fastify.get('/user/:userId', async (request, reply) => {
    const { userId } = request.params;
    reply.send(await addic7ed.getUser(userId));
});

fastify.get('/search', async (request, reply) => {
    const { query } = request.query;
    reply.send(await addic7ed.search(query));
});

fastify
    .listen(process.env['PORT'] || config.port, '0.0.0.0')
    .then(() => console.log('Server running'))
    .catch(console.error);